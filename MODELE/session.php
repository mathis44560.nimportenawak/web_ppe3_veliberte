<?php
/**
 * Created by PhpStorm.
 * User: malleaume
 * Date: 06/11/2018
 * Time: 15:15
 */

class session
{
    private $idc =null;

    public function __construct()
    {
        $this->idc = Connexion::connect();

    }

    public function getinfos(){
        if ($this->idc) {
            $req ="SELECT loginA, motDePasseA from adherent;";
            $result = $this->idc->query($req);
            Connexion::disconnect();
            return $result;
        }
    }
}