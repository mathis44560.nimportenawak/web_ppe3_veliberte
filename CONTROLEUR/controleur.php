<?php

require_once ('../MODELE/VeloModele.class.php');
require_once ('../MODELE/VeloElectriqueModele.class.php');

//permet à la VUE consultationVelosClassiques de récupérer la liste des vélos disponibles à la location
//pas besoin d'AJAX ici, cette récupération est faite au chargement de la page

function listeVelosClassiques(){
$VELOMod = new VeloModele();
return $VELOMod->getVelos(); //requete via le modele
}

function listeVelosClassiquesDisponibles()
{
$VELOMod = new VeloModele();
return $VELOMod->getVelosDispo(); //requete via le modele
}
function listeVelosElec(){
    $VELOEMod = new VeloElectriqueModele();
    return $VELOEMod->getVelosElec(); //requete via le modele
}

function listeVelosElecDisponibles()
{
    $VELOMod = new VeloElectriqueModele();
    return $VELOMod->getVelosElecDispo(); //requete via le modele
}
?>


